﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonAction : MonoBehaviour {

    [SerializeField]
    string _sceneName = "";
    public void LoadNextScene()
    {
        if(_sceneName!="")
        {
            SceneManager.LoadScene(_sceneName);
        }
    }


    [SerializeField]
    GameObject _prefab;
    GameObject _gameObject = null;
    public void GenerateGameObject()
    {
        if(_gameObject == null && _prefab!=null)
        {
            _gameObject = GameObject.Instantiate(_prefab);
        }
    }
}
