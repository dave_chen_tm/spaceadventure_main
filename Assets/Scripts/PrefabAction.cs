﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabAction : MonoBehaviour {

    [SerializeField]
    float _rotateSpeed = 1;
    void Update () {
        transform.RotateAround(Vector3.up, Time.deltaTime * _rotateSpeed);
	}
}
